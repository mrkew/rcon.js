"use strict";

/**
 * @fileOverview Allows you to send commands to Rcon-enabled servers.
 */

var {Socket} = java.net;
var {Byte} = java.lang;
var {ByteBuffer, ByteOrder} = java.nio;

export('RconError', 'buildPacket', 'Rcon');

/**
 * Create an RconError instance.
 * @param {String} message A description of the error (for humans).
 * @param {String} fileName The name of the file in which the error occurred.
 * @param {Number} lineNumber The line on which the error occurred.
 * @constructor
 */
function RconError(message, fileName, lineNumber) {
    Error.prototype.constructor.call(this, message, fileName, lineNumber);
}

RconError.prototype = Object.create(Error.prototype);

RconError.prototype.name = 'RconError';

/**
 * Build an Rcon packet. Throws an RconError if an argument has the wrong type.
 * @param {Number} id
 * @param {Number} type
 * @param {String} payload
 * @returns {java.nio.ByteBuffer}
 * @throws RconError
 */
function buildPacket(id, type, payload) {
    // note - we don't need to worry about id/type being integers, only numbers,
    // as Rhino converts JavaScript numbers to Java integers based on context.

    if (typeof id !== 'number') {
        throw new RconError("'id' should be a number.");
    }

    if (typeof type !== 'number') {
        throw new RconError("'type' should be a number.");
    }

    if (typeof payload !== 'string') {
        throw new RconError("'payload' should be a a string.");
    }

    // int(4 bytes) size
    // int(4 bytes) request id
    // int(4 bytes) type
    // variable length payload + null terminator (string bytes (ascii) + 1 byte)
    // empty string + null terminator (1 byte total)
    // = 14 bytes + string length (no null terminator)
    var packet = ByteBuffer.allocate(14 + payload.length)
        .order(ByteOrder.LITTLE_ENDIAN) // use little endian because
        // valve is idiotic, and doesn't want to use big endian like every other
        // decent network protocol
        .putInt(10 + payload.length) // size - why is it nine and not 10?!
        .putInt(id)
        .putInt(type)
        .put((new java.lang.String(payload)).getBytes('ascii'))
        .put(0x0) // null terminator for payload
        .put(0x0); // null terminator for empty string 

    packet.rewind(); // set the internal position back to 0

    return packet;
}

/**
 * Create an Rcon instance. Throws an RconError if an argument has the wrong
 * type.
 * An unfulfilled Rcon instance is essentially a container for information on
 * how to connect to the Rcon server. Connect to a server by using the connect()
 * function.
 * @param {String} host
 * @param {Number} port
 * @param {String} password
 * @param {Object} options A map of additional options.
 * <table>
 *  <tr><th>Name</th><th>Type</th><th>Default</th><th>Description</th></tr>
 *  <tr><td>timeout</td><td>int</td><td>0</td><td>The number of milliseconds
 *      after which the connected socket should disconnect if blocking, throwing
 *      a java.net.SocketTimeoutException. If 0, the socket will block
 *      indefinitely.</td></tr>
 * </table>
 * @throws RconError
 * @constructor
 * @see Rcon.prototype.connect
 */
function Rcon(host, port, password, options) {
    if (typeof host !== 'string') {
        throw new RconError("'host' must be a string.");
    }

    if (typeof port !== 'number' || port % 1 !== 0) {
        throw new RconError("'port' must be an integer.");
    }

    if (typeof password !== 'string') {
        throw new RconError("'password' must be a string.");
    }

    this._host = host;
    this._port = port;
    this._password = password;

    if (typeof options === 'object') {
        if (typeof options.timeout !== 'undefined') {
            if (typeof options.timeout !== 'number' || options.timeout % 1 !== 0) {
                throw new RconError("'options.timeout' must be an integer.");
            }

            this._timeout = options.timeout;
        }
    }

    this._sock = null;
    
    this._reqId = 0;
}

/**
 * Connect to the Rcon server. Sends a login packet and blocks until a reply is
 * received. Throws an RconError if login fails.
 * @throws RconError
 * @throws java.net.ConnectionException
 */
Rcon.prototype.connect = function() {
    this._sock = new Socket(this._host, this._port);
    var loginResp = this.send(3, this._password);

    if (typeof this._timeout !== 'undefined') {
        this._sock.setSoTimeout(this._timeout);
    }

    if (loginResp.type !== 2) {
        this.disconnect();

        throw new RconError("Login failed. (Invalid response code " + loginResp.type + ")");
    }
};

/**
 * Disconnect from the Rcon server.
 */
Rcon.prototype.disconnect = function() {
    this._sock.close();
};

/**
 * Transmit data bytes to the Rcon server. Throws an RconError if an argument
 * has the wrong type.
 * @param {java.nio.ByteBuffer}
 * @throws RconError
 * @see Rcon.prototype.send
 */
Rcon.prototype._transmit = function(data) {
    if (typeof this._sock === 'undefined' || ! this._sock.isConnected()) {
        throw new RconError("You must be connected to the Rcon server before you can transmit data.");
    }

    if ( ! (data instanceof ByteBuffer)) {
        throw new RconError("'data' must be a java.nio.ByteBuffer.");
    }

    var bytes = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, data.remaining());
    data.get(bytes);
    this._sock.getOutputStream().write(bytes);
};

/**
 * Receive a packet from the Rcon server. Blocks until all of the data is
 * received. Throws an RconError if this instance is not connected to an Rcon
 * server.
 * @returns An object having 'length', 'id', 'type', and 'payload' properties,
 * whose values are gleaned from the incoming Rcon packet. 
 * @throws RconError
 * @see Rcon.prototype.send
 */
Rcon.prototype._receive = function() {
    if (typeof this._sock === 'undefined' || ! this._sock.isConnected()) {
        throw new RconError("You must be connected to the Rcon server before you can receive data.");
    }

    var packet = {};
    
    var ar = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, 12);
    var buf = ByteBuffer.wrap(ar);
    buf.order(ByteOrder.LITTLE_ENDIAN);

    // hold the payload for parsing to an ascii string
    var payloadAr;
    var payloadBuf;

    this._sock.getInputStream().read(ar);
    packet.length = buf.getInt();
    packet.id = buf.getInt();
    packet.type = buf.getInt();

    payloadAr = java.lang.reflect.Array.newInstance(
            java.lang.Byte.TYPE,
            packet.length - 10);
    payloadBuf = ByteBuffer.wrap(payloadAr);
    payloadBuf.order(ByteOrder.LITTLE_ENDIAN);

    this._sock.getInputStream().read(payloadAr);

    payloadBuf.get(payloadAr);

    packet.payload = new java.lang.String(payloadAr, 'ascii');

    // skip the two null padding bytes
    this._sock.getInputStream().skip(2);

    return packet;
};

/**
 * Send a packet to the Rcon server, block until a reply is received, then
 * return that reply.
 * @param {Number} type
 * @param {String} payload A value of 3 indicates a login packet, while a value
 * of 2 indicates a command packet.
 * @returns An object having 'length', 'id', 'type', and 'payload' properties,
 * whose values are gleaned from the incoming Rcon packet. 
 * @throws RconError
 */
Rcon.prototype.send = function(type, payload) {
    var packet = buildPacket(this._reqId, type, payload);

    this._transmit(packet);
    
    this._reqId++;

    return this._receive();
};

/**
 * Send a command to the Rcon server, block until a reply is received, then
 * return that reply.
 * @param {String} command
 * @returns {String} The result of executing the command on the Rcon server.
 * @throws RconError
 */
Rcon.prototype.command = function(command) {
    return this.send(2, command).payload;
};
