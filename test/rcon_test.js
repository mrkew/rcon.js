"use strict";

var assert = require('assert');

var {ByteBuffer} = java.nio;

var rcon = require('../lib/rcon.js');

export('testConstructorValidation', 'testBuildPacketValidation');

function testConstructorValidation() {
    // 'host' should be a string
    assert.throws(function() {
        var r = new rcon.Rcon(42, 25564, 'password');
    }, rcon.RconError);

    // 'port' should be an integer
    assert.throws(function() {
        var r = new rcon.Rcon('localhost', 42.1337, 'password');
    }, rcon.RconError);

    // 'port' should be a number
    assert.throws(function() {
        var r = new rcon.Rcon('localhost', 'stringhere', 'password');
    }, rcon.RconError);

    // 'password' should be a string
    assert.throws(function() {
        var r = new rcon.Rcon('localhost', 25564, 9001);
    }, rcon.RconError);

    // 'options.timeout' should be an integer
    assert.throws(function() {
        var r = new rcon.Rcon('localhost', 25564, 'password', {
            timeout:    42.1337
        });
    }, rcon.RconError);
}

function testBuildPacketValidation() {
    // 'id' should be a number
    assert.throws(function() {
        rcon.buildPacket('stringhere', 3, "Hello, world!");
    }, rcon.RconError);

    // 'type' should be a number
    assert.throws(function() {
        rcon.buildPacket(3, 'stringhere', "Hello, world!");
    }, rcon.RconError);

    // 'payload' should be a string
    assert.throws(function() {
        rcon.buildPacket(3, 4, 5);
    }, rcon.RconError);
}

//var testConnect = function() {
//    var password = 'password';
//    var r = new rcon.Rcon('localhost', 25564, password);
//    var txBytes = null;
//
//    // JavaScript magic - override instance function
//    r.connect = function() { }
//
//    r.transmit = function(data) {
//        txBytes = data;
//    };
//
//    r.connect(); // expected to call transmit
// 
//    // note: if we use assert.equals and the two bytebuffers directly,
//    // apparently we get a BufferUnderrunError 
//    assert.isTrue(txBytes.equals(rcon.buildPacket(0, 3, password)));
//};

//var testTransmit = function() {
//    var r = new rcon.Rcon('localhost', 25564, 'password');
//
//    // replace connect with nothing
//    r.connect = function() {};
//
//    // we shouldn't be able to call transmit before connect
//    assert.throws(function() {
//        r.transmit(); // we don't need to supply anything - this should be
//        // checked first!
//    }, RconError);
//};

if (module === require.main) {
    system.exit(require('test').run(exports));
}
